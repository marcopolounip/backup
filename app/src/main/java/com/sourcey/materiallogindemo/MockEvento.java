package com.sourcey.materiallogindemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MockEvento extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_evento);
    }

    public void TelaMain(View view) {
        startActivity(new Intent(MockEvento.this, MainActivity.class));
        setContentView(R.layout.activity_main);
    }
}
