package com.sourcey.materiallogindemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EncontraCadastro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encontra_cadastro);
    }

    public void CriaCadastro(View view) {
        startActivity(new Intent(EncontraCadastro.this, MainActivity.class));
    }
}
